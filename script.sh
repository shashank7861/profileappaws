docker build --no-cache -t s1 .
CONTAINER_NAME="app1"
OLD="$(docker ps --all --quiet --filter=name="$CONTAINER_NAME")"
if [ -n "$OLD" ]; then
  docker stop $OLD
fi
docker run --rm -d -p 80:80 --name=app1 s1