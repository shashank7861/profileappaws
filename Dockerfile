 FROM node:latest as node
 WORKDIR /app
 COPY ./www .
# RUN npm install
# RUN npm audit fix
# RUN npm run build --prod

# stage 2
 FROM nginx:alpine
 COPY --from=node /app /usr/share/nginx/html
